import datetime


def date_time(func):
    def wrapper():
        print(datetime.datetime.now())
        func()

    return wrapper


@date_time
def say_hello():
    print("Hello")


say_hello()
