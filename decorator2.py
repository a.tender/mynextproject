from datetime import datetime


def run_only_between(_from=6, _to=23):
    def real_decorator(func):
        def wrapper():
            if _from <= datetime.now().hour < _to:
                func()
            else:
                pass

        return wrapper

    return real_decorator


@run_only_between(6, 23)
def say_hello():
    print("Hello")


say_hello()
